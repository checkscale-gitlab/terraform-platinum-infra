
data "packer_files" "docker_host_files" {
  file = "docker-host.pkr.hcl"
  file_dependencies = [
    "docker-host-playbook.yml",
  ]
}

data "packer_files" "nomad_files" {
  file = "nomad.pkr.hcl"
  file_dependencies = [
    "nomad-playbook.yml",
    "caddy/Caddyfile",
    "docker-compose.yaml",
    "consul/config-client.hcl",
    "consul/config-server.hcl",
    "nomad/config-client.hcl",
    "nomad/config-server.hcl",
  ]
}

resource "packer_image" "docker_host_image" {
  file  = data.packer_files.docker_host_files.file
  force = true
  variables = {
    hcloud_token  = var.hcloud_token
    image_name    = local.docker_host_image_name
    image_version = local.docker_host_image_version
  }

  triggers = {
    file_hash = data.packer_files.docker_host_files.files_hash
  }
}

resource "packer_image" "nomad_image" {
  file  = data.packer_files.nomad_files.file
  force = true
  variables = {
    hcloud_token              = var.hcloud_token
    image_name                = local.nomad_image_name
    image_version             = local.nomad_image_version
    infra_dir                 = local.infra_dir
    docker_host_image_name    = local.docker_host_image_name
    docker_host_image_version = local.docker_host_image_version
  }

  triggers = {
    file_hash = data.packer_files.nomad_files.files_hash
  }

  depends_on = [
    packer_image.docker_host_image,
  ]
}

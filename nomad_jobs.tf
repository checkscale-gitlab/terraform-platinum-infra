provider "nomad" {
  address   = "https://${local.nomad_main_server_dns_name}"
  http_auth = "${var.edge_auth_user}:${var.edge_auth_pw}"
}

# Register a job
resource "nomad_job" "hcloud_csi" {
  jobspec = <<-JOB
    job "hcloud_csi" {
      datacenters = ["dc1"]
      type = "system"

      group "node" {
        task "plugin" {
          driver = "docker"

          config {
            image = "hetznercloud/hcloud-csi-driver:1.5.3"
            privileged = true
          }

          env {
            HCLOUD_TOKEN="${var.hcloud_token}"
            CSI_ENDPOINT="unix:///csi/csi.sock"
          }

          csi_plugin {
            id        = "csi.hetzner.cloud"
            type      = "monolith"
            mount_dir = "/csi"
          }
        }
      }
    }
  JOB

  depends_on = [
    data.network_port_wait.main_node,
  ]
}

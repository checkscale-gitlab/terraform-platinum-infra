resource "tls_private_key" "ssh_key" {
  algorithm   = "ECDSA"
  ecdsa_curve = "P384"
}

resource "hcloud_ssh_key" "ssh_key" {
  name       = "Terraform Cloud Auto-Created"
  public_key = tls_private_key.ssh_key.public_key_openssh
}


variable "hcloud_token" {
  type      = string
  sensitive = true
}

variable "cloudflare_token" {
  type      = string
  sensitive = true
}

variable "dns_zone" {
  type = string
}

variable "edge_auth_user" {
  type = string
}

variable "edge_auth_pw_hash" {
  type      = string
  sensitive = true
}

variable "edge_auth_pw" {
  type      = string
  sensitive = true
}

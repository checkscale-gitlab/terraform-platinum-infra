
data "hcloud_image" "nomad_image" {
  with_selector = "name=${local.nomad_image_name},version=${local.nomad_image_version}"
  depends_on = [
    packer_image.nomad_image,
  ]
}

resource "hcloud_network_subnet" "nomad_subnet" {
  network_id   = hcloud_network.nomad_network.id
  type         = "cloud"
  network_zone = "eu-central"
  ip_range     = hcloud_network.nomad_network.ip_range
}

locals {
  nomad_is_server  = [for i in range(local.nomad_node_count) : i < local.nomad_server_count]
  nomad_node_types = [for i in range(local.nomad_node_count) : local.nomad_is_server[i] ? "server" : "client"]
}

resource "hcloud_server" "nomad_node" {
  count       = local.nomad_node_count
  name        = "nomad-node-${count.index}"
  image       = data.hcloud_image.nomad_image.id
  server_type = "cpx11"
  location    = "nbg1"
  labels = {
    build_uuid      = packer_image.nomad_image.build_uuid,
    nomad_is_server = local.nomad_is_server[count.index],
    node_type       = local.nomad_node_types[count.index],
  }
  firewall_ids = [hcloud_firewall.nomad_firewall.id]
  ssh_keys     = [hcloud_ssh_key.ssh_key.id]
  user_data = <<-CLOUDINIT
    #cloud-config
    fqdn: ${local.nomad_dns_names[count.index]}.

    write_files:
    - content: |
        NOMAD_DOMAIN=${local.nomad_dns_names[count.index]}
        CONSUL_DOMAIN=${local.consul_dns_names[count.index]}
        AUTH_USER=${var.edge_auth_user}
        AUTH_PW_HASH=${var.edge_auth_pw_hash}
      path: ${local.infra_dir}/post.env
    - encoding: b64
      content: ${
  base64encode(
    templatefile(
      "nomad/config-${local.nomad_node_types[count.index]}.hcl",
      {
        node_ip = local.nomad_ipv4_addrs[count.index],
      }
    )
  )
  }
      path: /etc/nomad.d/nomad.hcl
    - encoding: b64
      content: ${
  base64encode(
    templatefile(
      "consul/config-${local.nomad_node_types[count.index]}.hcl",
      {
        server_node_address = local.nomad_internal_dns_names[0],
        node_ip             = local.nomad_ipv4_addrs[count.index],
      }
    )
  )
}
      path: /etc/consul.d/consul.hcl
    runcmd:
      - [ "systemctl", ${local.nomad_is_server[count.index] ? "enable" : "disable"}, "--now", "infra" ]
  CLOUDINIT

network {
  network_id = hcloud_network.nomad_network.id
  ip         = local.nomad_ipv4_addrs[count.index]
}

depends_on = [
  hcloud_network_subnet.nomad_subnet,
]
}

locals {
  nomad_main_node_server = hcloud_server.nomad_node[0]
}

data "network_port_wait" "main_node" {
  host = local.nomad_main_server_dns_name
  port = 80

  depends_on = [
    local.nomad_main_node_server,
  ]
}

packer {
  required_plugins {
    hcloud = {
      version = ">= 1.0.0"
      source  = "github.com/hashicorp/hcloud"
    }
  }
}

variable "hcloud_token" {
  type = string
}

variable "image_name" {
  type = string
}

variable "image_version" {
  type = string
}

variable "infra_dir" {
  type = string
}

variable "docker_host_image_name" {
  type = string
}

variable "docker_host_image_version" {
  type = string
}

locals {
  name          = var.image_name
  version       = var.image_version
  snapshot_name = "${local.name}-${local.version}"
  infra_dir     = var.infra_dir
}

source "hcloud" "cfg" {
  token = var.hcloud_token
  image_filter {
    with_selector = [
      "name==${var.docker_host_image_name}",
      "version==${var.docker_host_image_version}",
    ]
  }
  location     = "nbg1"
  server_type  = "cpx11"
  ssh_username = "root"
}

build {
  source "hcloud.cfg" {
    server_name   = "${local.snapshot_name}-builder"
    snapshot_name = local.snapshot_name
    snapshot_labels = {
      name       = local.name
      version    = local.version
      created_by = "packer"
    }
  }
  provisioner "ansible-local" {
    playbook_file = "./nomad-playbook.yml"
  }
  provisioner "shell" {
    inline = ["systemctl enable nomad consul"]
  }

  provisioner "shell" {
    inline = [
      "mkdir -p -- '${local.infra_dir}/caddy'"
    ]
  }
  provisioner "file" {
    source      = "caddy/Caddyfile"
    destination = "${local.infra_dir}/caddy/Caddyfile"
  }
  provisioner "file" {
    source      = "docker-compose.yaml"
    destination = "${local.infra_dir}/docker-compose.yaml"
  }
  provisioner "file" {
    content     = <<-EOT
      INFRA_DIR=${local.infra_dir}
    EOT
    destination = "${local.infra_dir}/.env"
  }
  provisioner "file" {
    content     = <<-EOT
      [Unit]
      Description=Infra services
      [Service]
      Type=exec
      WorkingDirectory=${local.infra_dir}
      User=root
      Group=docker
      ExecStart=/usr/bin/docker compose up
      ExecStop=/usr/bin/docker compose down
      [Install]
      WantedBy=multi-user.target
    EOT
    destination = "/usr/lib/systemd/system/infra.service"
  }
}

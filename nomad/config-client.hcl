# Full configuration options can be found at https://www.nomadproject.io/docs/configuration

data_dir = "/opt/nomad/data"
bind_addr = "0.0.0.0"
log_level  = "INFO"

client {
  enabled = true
}

advertise {
  http = "${node_ip}"
  rpc  = "${node_ip}"
  serf = "${node_ip}"
}

plugin "docker" {
  config {
    allow_privileged = true
  }
}

#acl {
#  enabled = true
#}
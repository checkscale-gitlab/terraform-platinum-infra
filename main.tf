terraform {
  backend "remote" {
    organization = "platinum"
    workspaces {
      name = "platinum-infra"
    }
  }
  required_providers {
    packer = {
      source  = "toowoxx/packer"
      version = "0.8.1"
    }
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "~> 1.32.1"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.4.0"
    }
    nomad = {
      source  = "hashicorp/nomad"
      version = "~> 1.4.16"
    }
    network = {
      source  = "xdevs23/network"
      version = "0.0.1-beta.2"
    }
  }
}

provider "packer" {}

provider "hcloud" {
  token = var.hcloud_token
}

provider "cloudflare" {
  api_token = var.cloudflare_token
}

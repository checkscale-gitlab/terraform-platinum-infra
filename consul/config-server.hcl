
data_dir  = "/opt/consul/data"
server    = true
bootstrap = true
ui_config {
  enabled = true
}

datacenter = "dc1"
log_level  = "INFO"

bind_addr = "[::]"
bind_addr = "0.0.0.0"
advertise_addr = "${node_ip}"

connect {
  enabled = true
}

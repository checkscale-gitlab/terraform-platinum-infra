packer {
  required_plugins {
    hcloud = {
      version = ">= 1.0.0"
      source  = "github.com/hashicorp/hcloud"
    }
  }
}

variable "hcloud_token" {
  type = string
}

variable "image_name" {
  type = string
}

variable "image_version" {
  type = string
}

locals {
  name          = var.image_name
  version       = var.image_version
  snapshot_name = "${local.name}-${local.version}"
}

source "hcloud" "cfg" {
  token        = var.hcloud_token
  image        = "ubuntu-20.04"
  location     = "nbg1"
  server_type  = "cpx11"
  ssh_username = "root"
}

build {
  source "hcloud.cfg" {
    server_name   = "${local.snapshot_name}-builder"
    snapshot_name = local.snapshot_name
    snapshot_labels = {
      name       = local.name
      version    = local.version
      created_by = "packer"
    }
  }
  provisioner "shell" {
    inline = ["apt-get update -y && apt-get install -y ansible"]
  }
  provisioner "ansible-local" {
    playbook_file = "./docker-host-playbook.yml"
  }
}
